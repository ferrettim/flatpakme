**FlatpakMe**

FlatpakMe is a simple script to install Flatpaks on your system. The script adds a few repos before installing the appropriate flatpaks. Once software is installed, directory overrides are added for data access. The script itself is documented so read it for more information.

To use it, simply download and run it via terminal.

`wget https://gitlab.com/ferrettim/flatpakme/blob/master/flatpakme`

`sh flatpakme`

In a future version, the script may have branches for different distros, in particular, for Fedora Silverblue (my preferred distro), to install the same base packages as Fedora Workstation.
